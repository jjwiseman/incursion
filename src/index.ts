import * as fs from "fs";
import * as shapefile from "shapefile";
import * as turf from "@turf/turf";
import { AllGeoJSON, FeatureCollection, LineString, MultiPolygon, Point, Position, Properties } from "@turf/turf";
import { Feature, GeoJsonProperties, Polygon } from "geojson";
import { Logger, dummyLogger } from "ts-log";
import RBush, { BBox } from "rbush";
import _, { isArray } from "lodash";
import bbox from "@turf/bbox";
import booleanPointInPolygon from "@turf/boolean-point-in-polygon";


export type Airspace = Feature<Polygon | MultiPolygon, GeoJsonProperties>;

type BoundedAirspace = BBox & {
    airspace: Airspace
};

export type AirspaceDBStats = {
    numAirspaces: number;
    numQueries: number;
    numExpensiveChecks: number;
};

export type AirspaceDatabaseOptions = {
    logger?: Logger;
    writeDebugGeojson?: boolean;
};

export class AirspaceDatabase {
    index: RBush<BoundedAirspace>;
    stats: AirspaceDBStats;
    options: AirspaceDatabaseOptions;
    logger: Logger;

    constructor(options: AirspaceDatabaseOptions) {
        if (!options) {
            options = {};
        }
        this.options = options;
        this.logger = options.logger || dummyLogger;
        this.index = new RBush();
        this.stats = {
            numAirspaces: 0,
            numQueries: 0,
            numExpensiveChecks: 0
        };
    }

    addAirspaces(airspaces: Airspace[]): void {
        const boundedAirspaces = airspaces.map(airspace => {
            const [minX, minY, maxX, maxY] = bbox(airspace);
            const ba: BoundedAirspace = {
                airspace: airspace,
                ...{ minX, minY, maxX, maxY },
            };
            return ba;
        });
        this.index.load(boundedAirspaces);
        this.stats.numAirspaces += boundedAirspaces.length;
    }

    async loadAirspacesFromShapefile(shpPath: string, dbfPath: string): Promise<number> {
        const airspaces = await readAirspacesShapefile(shpPath, dbfPath);
        this.addAirspaces(airspaces);
        return airspaces.length;
    }

    getAirspacesAtCoords(coords: Coords): Airspace[] {
        const origin = coordsToPoint(coords);
        const [minX, minY] = origin.coordinates;
        const [maxX, maxY] = [minX, minY];
        const candidates = this.index.search({ minX, minY, maxX, maxY });
        this.stats.numQueries += 1;
        this.stats.numExpensiveChecks += candidates.length;
        const results = candidates.filter(ba => booleanPointInPolygon(origin, ba.airspace))
            .map(ba => ba.airspace);
        this.logger.debug(`Checked ${candidates.length} airspaces and found ${results.length} intersections`);
        return results;
    }

    getUpcomingAirspaces(coords: Coords, headingDegrees: number, speedKnots: number, lookaheadSeconds: number): Intersection[] {
        const origin = coordsToPoint(coords);
        const distanceKm = lookaheadSeconds * speedKnots * 0.514444 / 1000.0;
        const dest = turf.destination(origin, distanceKm, headingDegrees);
        const c = dest.geometry;
        if (c === null || c.type !== "Point") {
            throw new Error("Invalid endpoint: missing geometry");
        }
        const path = turf.lineString([origin.coordinates, c.coordinates]);
        this.logger.debug("Path: " + JSON.stringify(path));
        const [minX, minY, maxX, maxY] = turf.bbox(path);
        const candidates = this.index.search({ minX, minY, maxX, maxY });
        this.stats.numQueries += 1;
        this.stats.numExpensiveChecks += candidates.length;
        let intersections: Intersection[] = [];
        candidates.forEach(ba => {
            this.logger.debug(`********** Checking airspace ${airspaceDescriptionEnglish(ba.airspace)}`);
            const splitPath = splitPathByIntersections(path, ba.airspace);
            // The split path will have multiple features if the path intersects an
            // airspace polygon. The split path will have 0 features if it is either
            // entirely inside the polygon, or entirely outside. So to check whether a path
            // intersects an airspace polygone, we check whether it has at least 1 feature,
            // or if the origin is in the polygon.
            if (splitPath.features.length > 0 ||
                (booleanPointInPolygon(origin, ba.airspace))) {
                annotatePathWithDistances(splitPath);
                annotatePathWithTimes(splitPath, speedKnots);
                let newIntersections: Intersection[] = [];
                if (splitPath.features.length > 0) {
                    newIntersections = annotatedPathToIntersections(splitPath, ba.airspace);
                } else {
                    // This branch handles the case where the path stays
                    // entirely in an airspace, so there are no intersections.
                    newIntersections = [{
                        type: "Start",
                        airspace: ba.airspace,
                        coords: origin.coordinates,
                        distance: 0,
                        secs: 0
                    }];
                }
                if (this.options.writeDebugGeojson) {
                    const geojson = debugGeoJson(path, ba.airspace, splitPath, newIntersections);
                    const filename = airspaceDescriptionEnglish(ba.airspace).toLowerCase().replace(/W+/, "-") + ".geojson";
                    fs.writeFileSync(filename, JSON.stringify(geojson, null, 2));
                }
                intersections = intersections.concat(newIntersections);
            }
        });
        intersections = _.sortBy(intersections, i => i.distance);
        this.logger.debug(`Checked ${candidates.length} airspaces and found ${intersections.length} intersections`);
        return intersections;
    }

    getStats(): AirspaceDBStats {
        return this.stats;
    }
}

function debugGeoJson(path: turf.helpers.Feature<LineString>, airspace: Airspace, splitPath: FeatureCollection<LineString, Properties>, intersections: Intersection[]): AllGeoJSON {
    const transitionPoints = intersections.map(i => {
        const p = turf.point(jitterPosition(i.coords),
            {
                "marker-color": i.airspace.properties?.GLOBAL_ID === airspace.properties?.GLOBAL_ID ? "#FF0000" : "000000",
                "airspace": airspaceDescriptionEnglish(i.airspace),
                "type": i.type,
                "distance": i.distance,
                "secs": i.secs
            });
        return p;
    });
    splitPath.features.forEach(f => {
        if (!f.properties) {
            throw new Error("WTF");
        }
        f.properties.stroke = `#${f.properties.isInAirspace ? "FF00FF" : "000000"}`;
    });
    const debugGeoJson = turf.featureCollection([jitter(path), airspace, ...splitPath.features, ...transitionPoints]);
    return debugGeoJson;
}

function jitterPosition([lon, lat]: Position): Position {
    //return [lon, lat];
    return turf.randomPosition([lon - 0.01, lat - 0.01, lon + 0.01, lat + 0.01]);
}

function jitter<T extends Feature<Point | LineString | null, Properties>>(feat: T): T {
    const copy = _.cloneDeep(feat);
    if (!copy.geometry) {
        return copy;
    }
    switch (copy.geometry.type) {
        case "Point": {
            copy.geometry.coordinates = jitterPosition(copy.geometry.coordinates);
            break;
        }
        case "LineString": {
            copy.geometry.coordinates = copy.geometry.coordinates.map(jitterPosition);
            break;
        }
    }
    return copy;
}


export type IntersectionType = "Start" | "Enter" | "Leave";

export type Intersection = {
    airspace: Airspace;
    type: IntersectionType;
    coords: Position,
    distance: number;
    secs: number;
};

export function intersectionDescriptionEnglish(intersection: Intersection): string {
    if (intersection.type === "Start") {
        return `Starts in ${airspaceDescriptionEnglish(intersection.airspace)}`;
    } else {
        return `${intersection.type} ${airspaceDescriptionEnglish(intersection.airspace)} in ${intersection.distance.toFixed(1)} nm, ${intersection.secs.toFixed(1)} secs`;
    }
}

function annotatedPathToIntersections(path: FeatureCollection<LineString, Properties>, airspace: Airspace): Intersection[] {
    const intersections: Intersection[] = [];
    path.features.forEach((f, i) => {
        if (f.properties && _.has(f.properties, "distanceNm") && _.has(f.properties, "timeSecs")) {
            const isInAirspace = f.properties.isInAirspace as boolean;
            // Provisional intersection. We may replace the type later.
            const intersection: Intersection = {
                type: "Start",
                airspace: airspace,
                coords: f.geometry?.coordinates[0] as Position,
                distance: f.properties.distanceNm as number,
                secs: f.properties.timeSecs as number
            };
            if (i == 0) {
                if (!isInAirspace) {
                    // The first part of the path may not be in an airspace at all.
                    return;
                } else {
                    // Otherwise we're starting in the airspace.
                    intersection.type = "Start";
                    intersections.push(intersection);
                }
            } else {
                intersection.type = isInAirspace ? "Enter" : "Leave";
                intersections.push(intersection);
            }
        } else {
            console.error(f);
            throw new Error("Path is not annotated with distance and time");
        }
    });
    return intersections;
}

function splitPathByIntersections(line: turf.helpers.Feature<LineString>, airspace: Airspace): FeatureCollection<LineString, Properties> {
    const lineGeom = line.geometry;
    if (!lineGeom) {
        throw new Error("line is malformed");
    }
    const split = turf.lineSplit(line, airspace);
    reassembleLineSegments(lineGeom.coordinates[0], split);
    let isInAirspace = booleanPointInPolygon(lineGeom.coordinates[0], airspace);
    split.features.forEach(f => {
        if (!f.properties) {
            f.properties = {};
        }
        f.properties.isInAirspace = isInAirspace;
        f.properties.airspaceName = airspaceDescriptionEnglish(airspace);
        // Each feature alternates being in/out of the airspace polygon.
        isInAirspace = !isInAirspace;
    });
    return split;
}

// turf.lineSplit correctly splits the line, but the resulting segments
// can be in an odd order. See https://github.com/Turfjs/turf/issues/1989

function reassembleLineSegments(origin: Position, path: FeatureCollection<LineString, Properties>) {
    let candidateFeatures = path.features;
    const orderedFeatures: Feature<LineString, Properties>[] = [];
    while (candidateFeatures.length > 0) {
        candidateFeatures = _.sortBy(candidateFeatures, f => {
            if (f.geometry) {
                return turf.distance(origin, f.geometry.coordinates[0]);
            } else {
                throw new Error("Feature missing geometry");
            }
        });
        const closest = candidateFeatures.shift() as Feature<LineString, Properties>;
        origin = closest.geometry.coordinates[closest.geometry.coordinates.length - 1];
        orderedFeatures.push(closest);
    }
    path.features = orderedFeatures;
}


function annotatePathWithDistances(path: FeatureCollection<LineString, Properties>) {
    let distanceNm = 0;
    path.features.forEach(f => {
        if (!f.properties) {
            f.properties = {};
        }
        f.properties.distanceNm = distanceNm;
        distanceNm += turf.length(f, { units: "nauticalmiles" });
    });
}

function annotatePathWithTimes(path: FeatureCollection<LineString, Properties>, speedKnots: number) {
    path.features.forEach(f => {
        if (f.properties && _.has(f.properties, "distanceNm")) {
            f.properties.timeSecs = 3600 * f.properties.distanceNm / speedKnots;
        } else {
            console.error(f);
            throw new Error("Path not annotated with distances");
        }
    });
}

export type Coords = Point | [number, number] | { lat: number, lon: number };
// Or Coord.

function coordsToPoint(coords: Coords): Point {
    let cleanCoords: Point | null;
    if ("lat" in coords && "lon" in coords) {
        cleanCoords = turf.point([coords.lon, coords.lat]).geometry;
    } else if ("type" in coords && coords.type === "Point") {
        cleanCoords = coords;
    } else if (isArray(coords)) {
        cleanCoords = turf.point(coords).geometry;
    } else {
        throw new Error("coords must be Point or array or have .lon and .lat");
    }
    if (cleanCoords === null) {
        throw new Error("I don't know :(");
    }
    return cleanCoords;

}


// Reads the FAA "Class Airspace" shapefile, available at
// https://adds-faa.opendata.arcgis.com/datasets/c6a62360338e408cb1512366ad61559e_0

export async function readAirspacesShapefile(shpPath: string, dbfPath: string): Promise<Airspace[]> {
    const airspaces: Airspace[] = [];
    const source = await shapefile.open(shpPath, dbfPath);
    let item = await source.read();
    while (!item.done) {
        const type = item.value.geometry.type;
        if (type !== "Polygon" && type != "MultiPolygon") {
            throw new Error("Airspace is not defined with a Polygon or MultiPolygon: " + type);
        }
        const cleaned = turf.cleanCoords(item.value);
        airspaces.push(cleaned as Airspace);
        item = await source.read();
    }
    return airspaces;
}

export function airspaceDescriptionEnglish(airspace: Airspace): string {
    const p = airspace.properties;
    return (`${p?.NAME}, from ${p?.LOWER_VAL} ${p?.LOWER_UOM} ` +
        `${p?.LOWER_CODE} to ${p?.UPPER_VAL} ${p?.UPPER_UOM} ${p?.UPPER_CODE}`);
}
